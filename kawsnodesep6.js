const AWS = require('aws-sdk');
const sqs = new AWS.SQS();
const ddb = new AWS.DynamoDB.DocumentClient();
const mysql = require('mysql');

exports.handler = async (event) => {
    debugger;
    try {
        let data = await ddb.scan({
            TableName: "KChineseAnimal"
        }).promise();
        console.log(mysql);
        //console.log(data);

    } catch (err) {
        console.log(err);
        // error handling goes here
    };

    try {
        let data = await sqs.receiveMessage({
            QueueUrl: `https://sqs.${process.env.AWS_DEFAULT_REGION}.amazonaws.com/${process.env.SIGMA_AWS_ACC_ID}/KTestSQS`,
            MaxNumberOfMessages: 1,
            VisibilityTimeout: 30,
            WaitTimeSeconds: 0,
            AttributeNames: ['All']
        }).promise();
        //console.log(data);

    } catch (err) {
        console.log(err);
        // error handling goes here
    };

    return { "message": "Successfully executed" };
};